#include "mainworld.hpp"
#include "player.hpp"
#include "enemy.hpp"
#include <random>
#include <Tank/System/Entity.hpp>
#include <Tank/System/Keyboard.hpp>
#include <Tank/System/Game.hpp>
#include <boost/range/algorithm.hpp>
#include "startworld.hpp"

MainWorld::MainWorld()
{
    generate_enemies();
    player = makeEntity<Player>();
    score = makeEntity<Score>();

    connect(tank::Keyboard::KeyPress(tank::Key::Escape), [](){
        tank::Game::makeWorld<StartWorld>();
    });

    connect(tank::Keyboard::KeyPress(tank::Key::R), [](){
        tank::Game::makeWorld<MainWorld>();
    });
}

/*
void MainState::setup()
{
    generate_enemies();
    player = makeEntity<Player>();
}*/

void MainWorld::generate_enemies()
{
    auto windowSize = tank::Game::window()->getSize();
    std::mt19937 rg {std::random_device{}()};
    std::uniform_int_distribution<unsigned> randX {0,windowSize.x-32};
    std::uniform_int_distribution<unsigned> randY {0,windowSize.y/2};

    for (int i = 0; i < 10; ++i) {
        makeEntity<Enemy>(*this,tank::Vectorf({randX(rg), randY(rg)}));
    }
}

void MainWorld::update()
{
    World::update();
    auto it = boost::range::find_if(entities_, [](tank::observing_ptr<tank::Entity> e) {
            return e->isType("enemy");
    });

    if (it == entities_.end()) {
        generate_enemies();
    }
}
