#ifndef PLAYER_HPP
#define PLAYER_HPP
#include <Tank/System/Entity.hpp>

class Player : public tank::Entity
{
    std::unique_ptr<tank::EventHandler::Connection> c;
public:
    Player();

    virtual void onAdded() override;
    virtual void onRemoved() override;
    virtual void moveBy(tank::Vectorf vec) override;
};

#endif // PLAYER_HPP
