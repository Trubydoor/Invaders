#ifndef BULLET_HPP
#define BULLET_HPP
#include <Tank/System/Entity.hpp>
#include <Tank/Audio/SoundEffect.hpp>

class Bullet : public tank::Entity
{
    tank::Vectorf velocity;
    tank::SoundEffect sound {"assets/sounds/laser.ogg"};
    static tank::Image image;
public:
    Bullet(tank::Vectorf pos, tank::Vectorf velocity);

    void update() override;
};

#endif // BULLET_HPP
