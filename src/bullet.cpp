#include "bullet.hpp"
#include <Tank/Utility/Vector.hpp>
#include <Tank/Audio/SoundEffect.hpp>

tank::Image Bullet::image {"assets/images/bullet.tga"};

Bullet::Bullet(tank::Vectorf pos, tank::Vectorf velocity) : Entity(pos),
    velocity(velocity)
{
    this->setType("bullet");
    auto graph = makeGraphic(image);
    graph->setRotation(90);
    auto size = graph->getSize();
    this->setHitbox({0,0,size.x,size.y});
    sound.play();
}

void Bullet::update()
{
    if (offScreen()) {
        remove();
    }
    moveBy(velocity);

    auto collisions = collide();
    if (not collisions.empty()) {
        for (auto& e : collisions) {
            e->remove();
        }
        this->remove();
    }
}
