#ifndef STARTSTATE_HPP
#define STARTSTATE_HPP
#include <Tank/System/World.hpp>
#include "startscreen.hpp"

class StartWorld : public tank::World
{
    tank::observing_ptr<StartScreen> screen;
public:
    StartWorld();
};

#endif // STARTSTATE_HPP
