#include "startworld.hpp"
#include <Tank/System/Keyboard.hpp>
#include <Tank/System/Game.hpp>
#include "mainworld.hpp"

StartWorld::StartWorld()
{
    screen = makeEntity<StartScreen>();
    connect(tank::Keyboard::KeyPress(tank::Key::Space), [](){
        tank::Game::makeWorld<MainWorld>();
    });
}
