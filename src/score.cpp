#include "score.hpp"

Score::Score()
{
    this->setType("score");
    font.loadFromFile("assets/fonts/HardGothicNormal.ttf");
    text = this->makeGraphic<tank::Text>(font);
    text->setFontSize(72);
    text->setColor(tank::Color::White);
    text->setText("0");
    text->setPos({0,0});
    this->setLayer(5);
}

void Score::update()
{
    text->setText(std::to_string(score));
}
