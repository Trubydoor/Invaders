#include "particle.hpp"

tank::Image Particle::image {"assets/images/particle.tga"};

Particle::Particle(tank::Vectorf pos, tank::Vectorf velocity) :
    velocity{velocity}
{
    this->makeGraphic(image);
    this->setPos(pos);
    timer.start();
}

void Particle::update()
{
    moveBy(velocity);
    std::uniform_int_distribution<int> rand {0,50};
    if(timer.getTicks() > std::chrono::seconds(2)) {
        int x = rand(ran_gen);
        if (x < 1) {
            this->remove();
        }
    }
}
