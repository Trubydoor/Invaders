#ifndef SCORE_HPP
#define SCORE_HPP
#include <Tank/System/Entity.hpp>
#include <Tank/Utility/observing_ptr.hpp>
#include <Tank/Graphics/Text.hpp>

class Score : public tank::Entity
{
    tank::observing_ptr<tank::Text> text;
    tank::Font font;

public:
    Score();
    int score {0};

    virtual void update() override;

};

#endif // SCORE_HPP
