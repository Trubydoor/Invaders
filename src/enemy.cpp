#include "enemy.hpp"
#include "bullet.hpp"
#include <iostream>
#include <Tank/System/Keyboard.hpp>
#include "particle.hpp"
#include <thread>

tank::Image Enemy::image {"assets/images/enemy.tga"};

Enemy::Enemy(MainWorld& state, tank::Vectorf pos) : tank::Entity{pos}, state(state)
{
    this->setType("enemy");
    auto graph = makeGraphic(image);
    auto size = graph->getSize();

    setHitbox({0,0,size.x,size.y});
}

void Enemy::onRemoved()
{
    ++state.getScore()->score;
    std::uniform_real_distribution<float> rand1 {-1,1};
    std::uniform_real_distribution<float> rand2 {-2,2};

    for (int i = 0; i < 16; ++i) {
        state.makeEntity<Particle>(getPos(), tank::Vectorf{rand1(ran_gen),rand1(ran_gen)});
        state.makeEntity<Particle>(getPos(), tank::Vectorf{rand2(ran_gen),rand2(ran_gen)});
    }

    state.explode();
}

void Enemy::onAdded()
{
   // connect(tank::Keyboard::KeyPress(tank::Key::Tilde), [this](){this->remove();});
}

void Enemy::update()
{
    ++fc;

    std::uniform_int_distribution<int> rand {0,120};
    firing = rand(ran_gen) == 1 ? !firing : firing;

    if (fc % 30 != 0) {
        return;
    }

    auto size = getGraphic()->getSize();
    if (firing) {
        getWorld()->makeEntity<Bullet>(getPos() + tank::Vectorf{size.x/2, size.y},
                                       tank::Vectorf{0,5});
    }
}
