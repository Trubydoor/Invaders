#ifndef ENEMY_HPP
#define ENEMY_HPP
#include <Tank/System/Entity.hpp>
#include <random>
#include "mainworld.hpp"

class Enemy : public tank::Entity
{
    unsigned int fc = 0;
    bool firing = false;
    std::mt19937 ran_gen {std::random_device{}()};
    MainWorld& state;
    static tank::Image image;

public:
    Enemy(MainWorld& state, tank::Vectorf pos);
    void update() override;

    void onAdded() override;
    void onRemoved() override;
};

#endif // ENEMY_HPP
