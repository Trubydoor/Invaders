#ifndef MAINSTATE_HPP
#define MAINSTATE_HPP
#include <Tank/System/World.hpp>
#include <Tank/System/Entity.hpp>
#include <random>
#include "score.hpp"
#include <Tank/Audio/SoundEffect.hpp>

class Player;

class MainWorld : public tank::World
{
    tank::observing_ptr<Player> player;
    tank::observing_ptr<Score> score;
    tank::observing_ptr<tank::EventHandler::Connection> reset_con;
    void generate_enemies();
    void setup();

    tank::SoundEffect explosion {"assets/sounds/explosion.ogg"};

public:
    MainWorld();
    void update() override;
    tank::observing_ptr<Score> getScore() const {return score;}
    void explode() {explosion.play();}
};

#endif // MAINSTATE_HPP
