#ifndef PARTICLE_HPP
#define PARTICLE_HPP
#include <Tank/System/Entity.hpp>
#include <Tank/Utility/Timer.hpp>

class Particle : public tank::Entity
{
    tank::Vectorf velocity;
    tank::Timer timer;
    std::mt19937 ran_gen {std::random_device{}()};
    static tank::Image image;
public:
    Particle(tank::Vectorf pos, tank::Vectorf velocity);
    void update() override;
};

#endif // PARTICLE_HPP
