#include "player.hpp"
#include <Tank/System/Keyboard.hpp>
#include <Tank/System/Game.hpp>

#include "bullet.hpp"
#include "mainworld.hpp"

Player::Player()
{
    this->setType("player");
    auto graph = this->makeGraphic("assets/images/player.tga");
    auto windowSize = tank::Game::window()->getSize();
    this->setPos(tank::Vectorf((windowSize.x - graph->getSize().x)/2,
                               windowSize.y - graph->getSize().y));

    auto size = graph->getSize();
    this->setHitbox({0,0,size.x,size.y});
}

void Player::onAdded()
{
    using kbd = tank::Keyboard;
    using key = tank::Key;
    using namespace tank::literals;

    auto right = kbd::KeyDown(key::D) or kbd::KeyDown(key::Right);
    connect(right, &Player::moveBy, 5_x);

    auto left = kbd::KeyDown(key::A) or kbd::KeyDown(key::Left);
    connect(left, &Player::moveBy, -5_x);

    auto size = getGraphic()->getSize();

    auto fire_key = kbd::KeyPress(key::Space) or kbd::KeyPress(key::Up) or kbd::KeyPress(key::Z);
    auto fire = [this,size] {
        getWorld()->makeEntity<Bullet>(getPos() + tank::Vectorf{size.x/2,0},-5_y);
    };

    connect(fire_key, fire);

}

void Player::onRemoved()
{
    tank::Game::makeWorld<MainWorld>();
}

void Player::moveBy(tank::Vectorf vec)
{
    Entity::moveBy(vec, [this]{
        return getPos().x < 0 || getPos().x > 800 - getGraphic()->getSize().x;
    });
}

