#include <Tank/System/Game.hpp>
#include <Tank/Audio/Music.hpp>
#include "startworld.hpp"
#include "enemy.hpp"

int main()
{
    tank::Game::initialize({800,600});

    tank::Music music {"assets/sounds/music.ogg"};
    music.play();

    tank::Game::makeWorld<StartWorld>();

    tank::Game::run();
}
